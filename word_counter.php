<?php
    // --- input
    
    $text = "foo bar baz, foo-bar one. two- custom 
            field custom name 
            foo bar another custom
            last word ";
    
    // --- helpers     
    
    function _trim(string $word): ?string
    {
        $word = trim($word);
        return rtrim($word, ',.-');
    }
    
    function myFilter($var): ?bool
    {
        return $var !== "";
    }
   
    function count_words($atext)
    {
        $counter = [];
        $_max = 0;
        foreach(explode("\n", $atext) as $item) {
            $words_raw = array_filter(explode(" ", $item), 'myFilter');
            $word_cleaned = array_map('_trim', $words_raw);

            foreach($word_cleaned as $word) {
                if (array_key_exists($word, $counter)) {
                    $counter[$word] = $counter[$word] + 1;
                }
                else {
                    $counter[$word] = 1;
                }
                if ($counter[$word] > $_max) {
                    $_max = $counter[$word];
                }
            }
        }
        $result = array();
        foreach($counter as $word => $frequency) {
            if ($frequency == $_max) {
                array_push($result, $word);
            }
        }
        return array($result, $_max);
        
    }
    
    // --- running code here
    
    list($word, $freq) = count_words($text);
    
    echo "Word: ".implode($word, ', ')." Freq: ".$freq."\n";